// if you checked "fancy-settings" in extensionizr.com, uncomment this lines

// var settings = new Store("settings", {
//     "sample_setting": "This is how you use Store.js to remember values"
// });


chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		chrome.downloads.download({
				url: request,
				conflictAction: 'uniquify',
				filename: 'nfg-data.json'
		});

		sendResponse({farewell: "goodbye"});
		return true;
	});