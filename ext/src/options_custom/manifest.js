this.manifest = {
    "name": "Netflix Genres",
    "icon": "icon.png",
    "settings": [
        {
            "tab": "Details",
            "group": "Range",
            "name": "start",
            "type": "slider",
            "label": "Default starting point",
            "max": 99999,
            "min": 1,
            "step": 1,
            "display": true
        },
        {
            "tab": "Details",
            "group": "Range",
            "name": "limit",
            "type": "slider",
            "label": "Max number to capture",
            "max": 1000,
            "min": 0,
            "step": 1,
            "display": true
        },
        {
            "tab": "Details",
            "group": "Timing",
            "name": "delay",
            "type": "slider",
            "label": "Default delay",
            "max": 60,
            "min": 3,
            "step": 1,
            "display": true
        },
        {
            "tab": "Details",
            "group": "Timing",
            "name": "errorDelay",
            "type": "slider",
            "label": "How much longer to wait on errors",
            "max": 15,
            "min": 1.1,
            "step": 0.1,
            "display": true
        },
    ],
    "alignment": [
        [
            "start",
            "limit"
        ]
    ]
};
