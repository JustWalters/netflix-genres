var base = '/browse/genre/', hasRun = null,
start = 110000, limit = 50000, delay = 10 * 1000, exported = 109999;

function saveAndInc(val){
	"use strict";
	let index = location.pathname.split('/')[3], obj = {};
	const MAX_INCREMENT = 10;

	if (val && index > exported && val.title) {
		let key = 'nfg-' + index;
		obj[key] = val;
		chrome.storage.local.set(obj);
	}

	index = Number(index) + Math.floor(Math.random() * MAX_INCREMENT);
	if (start <= index && index <= start + limit) {
		setTimeout(function(){
			location.pathname = base + index;
		}, delay);
	}
}

function downloadData(keys){
	"use strict";
	chrome.storage.local.get(null, function(items) { // null implies all items
    // Convert object to a string.
    let result = JSON.stringify(items);

    // Save as file
    let url = 'data:application/json;base64,' + btoa(result);
    chrome.runtime.sendMessage(url, function(response) {
		  console.log(response);
		});
	});
}

function deleteOldData(lowLimit, highLimit){
	"use strict";
	// lowLimit = 1;
	// highLimit = 10;
	let keys = [];
	if (highLimit <= lowLimit) return;

	for (let i = lowLimit; i <= highLimit; i++) {
		keys.push('nfg-' + i);
	}

	chrome.storage.local.remove(keys, function() {
		printData();
	});

}

jQuery(window).load(function(){
	if (location.href.indexOf(base) < 0) { //haven't started running loop
		printData();

		var run = confirm('start?');
		if (!run) return;

		var shouldDL = confirm('download?');
		if (shouldDL) downloadData();

		var shouldDelete = confirm('delete?');
		if (shouldDelete) {
			// get limit
			var low = prompt('low?') || 1;
			deleteOldData(low, exported);
		}

		start = prompt('where?');
		if (!start) return;

		location.pathname = base + Number(start);
		hasRun = true; // not sure about this, but shouldn't ever be a problem
	} else {
		var myObj = {
			title: document.getElementsByClassName('genreTitle')[0] && document.getElementsByClassName('genreTitle')[0].textContent,
			num: document.getElementsByClassName('video-artwork').length
		};
		saveAndInc(myObj);
		hasRun = true;
	}
});

setTimeout(function handleError(){
	"use strict";
	if (hasRun) {
		console.log('waited 15 seconds, but still here');
	} else { //"Netflix Site Error" most likely
		saveAndInc(null);
	}
}, 1.5 * delay);

function printData(which, logValue){
	which = which || null;
	logValue = logValue || 12;
	chrome.storage.local.get(which, function(i){console.log(logValue,i);});
}
